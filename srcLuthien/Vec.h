#ifndef VEC_H_
#define VEC_H_
#include "defines.h"
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
using  ulog = unsigned long;
// *** vec2 ****
template <typename T>
struct vec2 {
    vec2 (const T u, const T v) : d{u,v} {}
    vec2 (const T a[2]) : d{a[0],a[1]} {}
    vec2 (): d{0,0} {}
    T& operator()(int i) {return d[i];}
    const T& operator()(int i) const {return d[i];}
    vec2<T>& operator=(double s) {d[0]=s;d[1]=s;return (*this);}
    vec2<T>& operator+=(vec2<T> o) {d[0]+=o(0);d[1]+=o(1);return(*this);}
    vec2<T>& operator-=(vec2<T> o) {d[0]-=o(0);d[1]-=o(1);return(*this);}
    vec2<T> operator/(double s) {vec2<T>o; o(0)=d[0]/s;o(1)=d[1]/s;return o;}
    vec2<T> operator/=(double s) {d[0]/=s;d[1]/=s;return (*this);}

    //dot product of two vectors
    friend T dot(const vec2<T> &v1, const vec2<T> &v2) {
        T s=0;  for (int i=0;i<2;i++) s+=v1(i)*v2(i);
        return s;   }

    //vector magnitude
    friend T mag(const vec2<T> &v) {return sqrt(dot(v,v));}

    //unit vector
    friend vec2<T> unit(const vec2<T> &v) {return vec2(v)/mag(v);}

    T& x() {return d[0];}
    T x() const {return d[0];}
    T& y() {return d[1];}
    T y() const {return d[1];}
protected:
    T d[2];
};

//vec2-vec2 operations
template<typename T>    //addition of two vec3s
vec2<T> operator+(const vec2<T>& a, const vec2<T>& b) {
    return vec2<T> (a(0)+b(0),a(1)+b(1));   }
template<typename T>    //subtraction of two vec2s
vec2<T> operator-(const vec2<T>& a, const vec2<T>& b) {
    return vec2<T> (a(0)-b(0),a(1)-b(1));   }
template<typename T>    //element-wise multiplication of two vec2s
vec2<T> operator*(const vec2<T>& a, const vec2<T>& b) {
    return vec2<T> (a(0)*b(0),a(1)*b(1));   }
template<typename T>    //element wise division of two vec3s
vec2<T> operator/(const vec2<T>& a, const vec2<T>& b) {
    return vec2<T> (a(0)/b(0),a(1)/b(1));   }

//vec2 - scalar operations
template<typename T>        //scalar multiplication
vec2<T> operator*(const vec2<T> &a, T s) {
    return vec2<T>(a(0)*s, a(1)*s);}
template<typename T>        //scalar multiplication 2
vec2<T> operator*(T s,const vec2<T> &a) {
    return vec2<T>(a(0)*s, a(1)*s);}

//output
template<typename T>    //ostream output
std::ostream& operator<<(std::ostream &out, vec2<T>& v) {
    out<<v(0)<<" "<<v(1)<<" 0"; //paraview does not support 2-component arrays
    return out;
}

using double2 = vec2<double>;
using long2 = vec2<long>;
using int2 = vec2<int>;

template <typename T>
struct vec3 {
    vec3 (const T u, const T v, const T w) : d{u,v,w} {}
    vec3 (const T a[3]) : d{a[0],a[1],a[2]} {}
    vec3 (): d{0,0,0} {}

    T& operator()(int i) {return d[i];}
    T operator()(int i) const {return d[i];}
    vec3<T>& operator=(double s) {d[0]=s;d[1]=s;d[2]=s;return (*this);}
    vec3<T>& operator+=(vec3<T> v) {d[0]+=v(0);d[1]+=v(1);d[2]+=v(2);return(*this);}
    vec3<T>& operator-=(vec3<T> v) {d[0]-=v(0);d[1]-=v(1);d[2]-=v(2);return(*this);}
    vec3<T> operator/(double s) {vec3<T> v; v(0) = d[0] / s; v(1) = d[1] / s; v(2) = d[2] / s; return v;}
    vec3<T> operator/=(double s) {d[0]/=s;d[1]/=s;d[2]/=s;return (*this);}

    //dot product of two vectors
    friend T dot(const vec3<T> &v1, const vec3<T> &v2) {
        T s=0;  for (int i=0;i<3;i++) s+=v1(i)*v2(i);
        return s;   }

    //vector magnitude
    friend T mag(const vec3<T> &v) {return sqrt(dot(v,v));}

    //unit vector
    friend vec3<T> unit(const vec3<T> &v) {if (mag(v)>0) return vec3(v)/mag(v); else return {0,0,0};}

    //cross product
    friend vec3<T> cross(const vec3<T> &a, const vec3<T> &b) {
        return {a(1)*b(2)-a(2)*b(1), a(2)*b(0)-a(0)*b(2), a(0)*b(1)-a(1)*b(0)};
    }

    T& x() {return d[0];}
    T x() const {return d[0];}
    T& y() {return d[1];}
    T y() const {return d[1];}
    T& z() {return d[2];}
    T z() const {return d[2];}
    T& r() {return d[1];}
    T r() const {return d[1];}
    T& p() {return d[2];}
    T p() const {return d[2];}
protected:
    T d[3];
};

//vec3-vec3 operations
template<typename T>    //addition of two vec3s
vec3<T> operator+(const vec3<T>& a, const vec3<T>& b) {
    return vec3<T> (a(0)+b(0),a(1)+b(1),a(2)+b(2)); }
template<typename T>    //subtraction of two vec3s
vec3<T> operator-(const vec3<T>& a, const vec3<T>& b) {
    return vec3<T> (a(0)-b(0),a(1)-b(1),a(2)-b(2)); }
template<typename T>    //element-wise multiplication of two vec3s
vec3<T> operator*(const vec3<T>& a, const vec3<T>& b) {
    return vec3<T> (a(0)*b(0),a(1)*b(1),a(2)*b(2)); }
template<typename T>    //element wise division of two vec3s
vec3<T> operator/(const vec3<T>& a, const vec3<T>& b) {
    return vec3<T> (a(0)/b(0),a(1)/b(1),a(2)/b(2)); }

//vec3 - scalar operations
template<typename T>        //scalar multiplication
vec3<T> operator*(const vec3<T> &a, T s) {
    return vec3<T>(a(0)*s, a(1)*s, a(2)*s);}
template<typename T>        //scalar multiplication 2
vec3<T> operator*(T s,const vec3<T> &a) {
    return vec3<T>(a(0)*s, a(1)*s, a(2)*s);}

//output
template<typename T>    //ostream output
std::ostream& operator<<(std::ostream &out, const vec3<T>& v) {
    out<<v(0)<<" "<<v(1)<<" "<<v(2);
    return out;
}

using double3 = vec3<double>;
using int3 = vec3<int>;
using long3 = vec3<long>;
template <typename T> 
struct Array3D{

    Array3D(long n1, long n2, long n3){
       allocate(n1,n2,n3);
    }
    Array3D(const long3& nn){
       allocate(nn(0),nn(1),nn(2));
    }
    Array3D(Array3D &&other): _data{other._data}, 
                  _size{other._size}{
        other._data = nullptr;
        other._size =  0; 
    }
    Array3D(const Array3D &other): _size{other._size} { 
       	allocate( _size(0),_size(1),_size(2) );
    	for(auto i = 0; i < capacity(); ++i){
            _data[i] = other._data[i] ;
        }
    }
    Array3D(){
        _data = nullptr;
        _size = 0.;
    }
    
    void allocate(long n1, long n2, long n3){
        _data = new T[ n1 * n2 * n3];
        _size = long3(n1,n2,n3);
    }
    
    void clear(){
        for(auto i = 0; i < capacity(); ++i){
            _data[i] = 0.;
        }
    }

    void erase(){
        if (_data != nullptr)
            delete[] _data;
        _size = 0.;
    }

    ~Array3D(){
       erase();
    }
    
    Array3D<T>& operator=(T s) {
        for(auto i = 0; i < capacity(); ++i){
            _data[i] = s ;
        }
        return (*this);
    }
    
    Array3D<T>& operator=(const Array3D<T>& array3D) {
    	// Self-assignment check
    	if (this == &array3D)
        	return *this;
        // Checking the conformity of dimensions
       	assert( capacity() == array3D.capacity() );
        for(auto i = 0; i < capacity(); ++i){
            _data[i] = array3D._data[i] ;
        }
        return (*this);
    }   
    Array3D<T>& operator=(Array3D<T> &&array3D) {
    	// Self-assignment check
    	if (this == &array3D)
        	return *this;
        // Checking the conformity of dimensions
       	assert( capacity() == array3D.capacity() );
		delete[] _data;
       	_data = array3D._data;
       	_size = array3D._size;
        array3D._data = nullptr;
        array3D._size =  0; 
        return (*this);
    }   

    Array3D<T>& operator-=(const Array3D<T>& array3D) {
        assert( capacity() == array3D.capacity() );
        for(auto i = 0; i < capacity(); ++i){
            _data[i] -= array3D._data[i] ;
        }
        return (*this);
    }
    
    Array3D<T>& operator+=(const Array3D<T>& array3D) {
        assert( capacity() == array3D.capacity() );
        for(auto i = 0; i < capacity(); ++i){
            _data[i] += array3D._data[i] ;
        }
        return (*this);
    }          
    
    T& operator() (long i, long j, long k) {
        assert( capacity() > i * _size(1) * _size(2) + j * _size(2) + k &&"3D\n" );

    	return _data[i * _size(1) * _size(2) + j * _size(2) + k ];
    }

    const T& operator() (long i, long j, long k) const{ 
        assert( capacity() > i * _size(1) * _size(2) + j * _size(2) + k &&"3D\n" );

    	return _data[i * _size(1) * _size(2) + j * _size(2) + k];
    }
    
    T& data(long i) {return _data[i];}
    const T& data(long i) const {return _data[i];}

    long3 size() const{
        return _size;
    }
    long capacity() const{
        return _size(0)*_size(1)*_size(2);
    }

protected:
    T* _data;
    long3 _size;
};

template <typename T> 
struct Array2D{

    Array2D(long n1, long n2){
	   allocate(n1,n2);
    }
    Array2D(long2 nn){
       allocate(nn(0),nn(1));
    }
    Array2D(Array2D &&other): _data{other._data}, _size{other._size}{
        other._data = nullptr;
        other._size = 0; 
    }
    Array2D(){
        _data = nullptr;
    	_size = 0.;
    }
	
    void allocate(long n1, long n2){
    	_data = new T[ n1 * n2];
    	_size =  long2(n1,n2);
    }
    
    void clear(){
        for(auto i = 0; i < capacity() ; ++i){
            _data[i]=0.;
        }

    }
	void erase(){
        if (_data != nullptr)
            delete[] _data;
        _size = 0.;
    }
    ~Array2D(){
	   erase();
    }
    Array2D<T>& operator=(T s) {
        for(auto i = 0; i < capacity(); ++i){
            _data[i] = s ;
        }
        return (*this);
    }	
    T& operator() (long i, long j) {
    assert( capacity() > i * _size(1)  + j &&"2D\n" );

	return _data[i * _size(1) + j];
    }

    const T& operator() (long i, long j) const{	
        assert( capacity() > i * _size(1)  + j &&"2D\n" );
	return _data[i * _size(1) + j];
    }
    
    T& data(long i) {return _data[i];}
    const T& data(long i) const {return _data[i];}
  
    long2 size() const{
        return _size;
    }
    long capacity() const{
        return _size(0)*_size(1);
    }
    T sum_d2(long i) const{
        T sum = 0;
        for( long t =  0; t < _size(1); ++t){
          sum += _data[i * _size(1) + t];
        }
        return sum;
    }

protected:
    T* _data;
    long2 _size;
};

template <typename T> 
struct Array1D{
    T *_data;
    long _size;
    
    Array1D(std::size_t sizeDim){
       allocate(sizeDim);
    }
    Array1D(Array1D &&other): _data{other._data}, _size{other._size} {
        other._data = nullptr;
        other._size = 0; 
    }
    Array1D(){
        _size = 0.;
    }
    
    void allocate(long sizeDim){
        _data = new T[ sizeDim];
        _size =  sizeDim;
    }
    
    void clear(){
        for(auto i = 0; i<_size; ++i){
            _data[i]=0.;
        }
    }
    T& data(long i) {return _data[i];}
    const T& data(long i) const {return _data[i];}    
    void erase(){
        delete[] _data;
        _size = 0.;
    }
    long capacity() const{
        return _size;
    }
    Array1D<T>& operator=(T s) {
        for(auto i = 0; i <_size; ++i){
            _data[i] = s ;
        }
        return (*this);
    }

    ~Array1D(){
        erase();
    }
    
    T& operator() (long i) {
                assert( capacity() > i &&"1D\n" );
        return _data[i];
    }

    const T& operator() (long i) const{ 
        return _data[i];
    }
    long size() const{
        return _size;
    }
};

template <typename T> 
struct Array{

    Array(long maxSize){
		allocate(maxSize);
		_size = 0;
    }
	
    Array(){
        _data = nullptr;
		_maxSize = 0;
		_size = 0;
    };
    Array(Array &&arr): _data(arr._data), _size(arr._size), _maxSize(arr._maxSize) {
        arr._data = nullptr;
        arr._size = 0;
        arr._maxSize = 0;
    }
    ~Array(){
		erase();
    }
		
    void allocate(long maxSize){
		_data = new T[maxSize];
		_maxSize = maxSize;
    }

    void clear(){
        for (auto i = 0; i < _maxSize;++i)
		  _data[i] = 0.;
    }	
	
    void erase(){
        if( _data!= nullptr)
            delete[] _data;
    }   
    

	void add(const T& elem){
	    _data[_size] = elem;
	    ++_size;
        assert( capacity() > _size &&"1D add\n" );
	}
    
    void del(long k){
        assert( k>=0 && k < _size &&"1D add\n" );
        
        _data[k] = _data[_size-1];
  		--_size;
    }
    long size() const{
    	return _size;
    }
    long& size(){
        return _size;
    }
    long capacity() const{
        return _maxSize;
    }
    T& operator() (long i) {
        assert( capacity() > i &&"1D\n" );

		return _data[i];
    }

    const T& operator() (long i) const{
        assert( capacity() > i &&"1D\n" );
		return _data[i];
    }
    T pop(){
        --_size;
        return _data[_size];
    }

        T* _data;
        long _size, _maxSize;
};


#endif 
