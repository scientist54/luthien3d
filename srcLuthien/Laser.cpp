#include "Laser.h"
#include "const.h"

inline double pow2(double a){
  return a*a;
}
inline double pow3(double a){
  return a*a*a;
}
inline double pow4(double a){
  return a*a*a*a;
}

Laser::Laser(const std::vector<std::string>& vecStringParams){
    for (const auto& line: vecStringParams){
        set_params_from_string(line);
    }

}

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void Laser::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="tau"){
        tau = stod(strvec[1]);
    }

    if(strvec[0]=="w0"){
        w0 = stod(strvec[1]);
    }
    if(strvec[0]=="type"){
        type = strvec[1];
    }
    if(strvec[0]=="y0"){
        y0 = stod(strvec[1]);
    }
    if(strvec[0]=="vg"){
       vg =  stod(strvec[1]);
    }
    if(strvec[0]=="sigma0"){
       sigma0 =  stod(strvec[1]);
    }
    if(strvec[0]=="focus_x"){
        focus_x = stod(strvec[1]);
    }
    if(strvec[0]=="start_x"){
        start_x = stod(strvec[1]);
    }
    if(strvec[0]=="a0"){
       a0 = stod(strvec[1]);
    }
    if(strvec[0]=="delay"){
        delay = stod(strvec[1]);
    }
   
}

bool Laser::is_work(long timestep) const{
  double cTime = timestep*Dt;

  return  (cTime >= delay && cTime <= delay + 2 * tau);

}

double3 Laser::force(double3 x, long timestep) const{
   double3 f;
   /*double z = x(0);
   double r = x(1);
   double startLas;
   double cTime = timestep*Dt;

    startLas = (z - start_d1) / vg + delay; 

    if( cTime  >= startLas && cTime  <= startLas + 2 * tau) {
      const double RR = 0.5 * w0 * sigma0 * sigma0;
      const double sigma = sigma0 * sqrt(1. + pow2( (z - focus_d1) / RR) );
      const double w = 0.5*PI*(cTime - startLas) / tau;
      const double Psin = sin(w);
      const double Pcos = cos(w);
      f(0) =  0.5 * PI * pow2(a0) / tau / vg * pow2(sigma0) / pow2(sigma) * pow3(Psin) * Pcos * exp(-2.*pow2(r / sigma));
      f(1) = r * pow2(a0) * pow2(sigma0) / pow4(sigma) * pow4(Psin) * exp(-2. * pow2(r / sigma));
  }*/
   return f;

}
double3 Laser::get_field_coll(double3 x, long timestep) const{
  double f = 0.;
  /*double z = x(0);
  double r = x(1);
  double startLas;
  double cTime = timestep*Dt;

  startLas = (z - start_d1) / vg + delay; 
  //    std::cout << z << " " <<  start_z << " "<<vg << " " <<delay  << "\n";

  if( cTime >= startLas && cTime <= startLas + 2 * tau) {
      const double RR = 0.5 * w0 * sigma0 * sigma0;
      const double sigma = sigma0 * sqrt(1. + pow2( (z - focus_d1) / RR) );
      const double w = 0.5*PI*(cTime - startLas) / tau;

      f =  a0 * w0 * (sigma0 / sigma) * exp( - pow2(r / sigma) ) * pow2(sin(w));
  }*/
   return double3(0.,f,0.);

}

double Laser::get_Ez(double y, double t) const{
 
    const double s2 = exp( -(y - y0) * (y - y0) / (sigma0 * sigma0) ) * cos(w0 * t);

    const double s1 = sin(0.5 * PI * t / tau);
    
    return a0 * w0 * s1 * s1 * s2;
}
double Laser::get_Ey(double x, double y, long timestep) const{
    double ctime = timestep*Dt - delay;
    double s2;
    if(focus_x >= 0.){
      x -= focus_x;
      const double Rlenght = 0.5 * w0 * sigma0 * sigma0;
      const double wx = sigma0 * sqrt(1. + x * x / (Rlenght * Rlenght) );
      const double phi = atan( x / Rlenght);
      const double R = x * (1. + Rlenght * Rlenght / (x * x));
      s2 = sqrt(sigma0 / wx) * exp( - (y - y0) * (y - y0) / ( wx * wx)) 
                        * cos(w0 * ctime + phi - w0 * (y - y0) * (y - y0) * 0.5 / R);
    }
    else{
      s2 = exp( -(y - y0) * (y - y0) / (sigma0 * sigma0) ) * sin(w0 * ctime);

    }

    const double s1 = sin(0.5 * PI * ctime / tau);
    
    return a0 * w0 * s1 * s1 * s2;

}
