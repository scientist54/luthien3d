#ifndef MESH_H_
#define MESH_H_
#include "World.h"
#include "Laser.h"
#include <mpi.h>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

double calc_energy_field(const Array3D<double3>& field);
void exchange_fieldsE(Array3D<double3>& field,const MPI_Topology &MPIconf);

struct Mesh{
    Mesh(const World& world);

    Array3D<double3> fieldE;
    Array3D<double3> fieldB;
   // Array3D<double3> fieldEx;
   // Array3D<double3> fieldBx;
   // Array3D<double3> fieldEy;
   // Array3D<double3> fieldBy;
   // Array3D<double3> fieldEz;
   // Array3D<double3> fieldBz;        
    Array3D<double3> fieldJ;
    Array3D<double3> fieldB0;
    std::vector<Laser> lasers;
    
    void set_fields();

    void set_uniform_fields();
    void read_from_recovery(const MPI_Topology &MPIconf);
    void write_recovery(const MPI_Topology &MPIconf) const;
    void update(long timestep);
    double3 get_fieldE_in_pos(const double3& r) const;
    double3 get_fieldE_in_cell(long i, long j, long k)  const;
    double3 get_fieldB_in_cell(long i, long j, long k)  const;
    double get_fieldE_energy() const{
        return calc_energy_field(fieldE);
    };
    void laser_source(long timestep);
    double get_fieldB_energy() const{
        return calc_energy_field(fieldB)-calc_energy_field(fieldB0);
    };
    void reduce_current(const MPI_Topology &MPIconf);
    ~Mesh(){
    }
protected:
    const World &_world;
};


#endif 
