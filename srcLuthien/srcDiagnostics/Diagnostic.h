#ifndef DIAGNOSTIC_H_
#define DIAGNOSTIC_H_
#define DIAGNOSTIC_H_
#include "World.h"
#include "Mesh.h"
#include "Particles.h"
#include "Timer.h"
void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf);
void write_array3D(const Array3D<double>& data, long size1, long size2, long size3, const char* filename, const MPI_Topology& MPIconf);



template <typename T>
struct BoundData{
    T x_min, x_max;
    T y_min, y_max;
    T z_min, z_max;
    BoundData(){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region, long n){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    ~BoundData(){};
    void clear(){
        x_min = 0.;
        x_max = 0.;
        y_min = 0.;
        y_max = 0.;
        z_min = 0.;
        z_max = 0.;
    };
};
template<>
inline BoundData<Array1D<double> >::BoundData(const Region &region): x_min(region.numNodes.z() ),x_max(region.numNodes.z() ),
                                                                     y_min(region.numNodes.x() ),y_max(region.numNodes.x() ),
                                                                     z_min(region.numNodes.x() ),z_max(region.numNodes.x() ) {
        clear();
}
template<>
inline BoundData<Array2D<double> >::BoundData(const Region &region, long n): x_min(region.numNodes.y() ,n),x_max(region.numNodes.y() ,n),
                                                                     y_min(region.numNodes.x() ,n),y_max(region.numNodes.x() ,n) {
        clear();
}
template<>
inline void BoundData<Array2D<double3> >::clear(){
        x_min = double3(0.,0.,0.);
        x_max = double3(0.,0.,0.);
        y_min = double3(0.,0.,0.);
        y_max = double3(0.,0.,0.);
}

template<>
inline BoundData<Array2D<double3> >::BoundData(const Region &region, long n):x_min(region.numNodes.y() ,n),x_max(region.numNodes.y() ,n),
                                                                     y_min(region.numNodes.x() ,n),y_max(region.numNodes.x() ,n) {
        clear();
}
template<>
inline BoundData<double>::BoundData(){
        clear();
};
template<>
inline BoundData<double3>::BoundData(){
        clear();
};

struct DiagData{

    
    DiagData(const Region &region) :powerRadLine(region ) {
                            //powerRadAvgLine(region,long(2*PI/Dt)),
                            //fieldEAvgLine(region,long(2*PI/Dt)),
                            //fieldBAvgLine(region,long(2*PI/Dt)){
       Reset() ;
    };

    void calc_energy(const Mesh &mesh,const std::vector<ParticlesArray> &species);
    void calc_radiation_Pointing(const Mesh &mesh, const Region &region);
    void calc_fields_avg_line(const Mesh& mesh,const World& world,long timestep);
    void calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep);
    
    void Reset(){
        //powerRad.clear();
        //powerRadAvg.clear();
        //powerRadLine.clear();
    };

    BoundData<double> powerRad;
    BoundData<double> powerRadAvg;
    BoundData<Array1D<double> > powerRadLine;

    //BoundData< Array2D<double> > powerRadAvgLine;
    //BoundData< Array2D<double3> > fieldEAvgLine;
    //BoundData< Array2D<double3> > fieldBAvgLine;

    std::map<std::string,double> energyParticlesKinetic;
    std::map<std::string,double> energyParticlesInject;
    
    double energyFieldE, energyFieldB;

};




struct Writer{
protected:
    const World &_world;
    const Mesh &_mesh;
    const std::vector<ParticlesArray> &_species;
public:
    FILE *fDiagEnergies;
    DiagData diagData;

    Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species);

    void output( long timestep);
    ~Writer(){
        if( !_world.MPIconf.is_master() ) return;
            fclose(fDiagEnergies);  
    } 

    void write_particles2D(long timestep);
    void write_particles3D(long timestep);
    void write_energies(long timestep);
    void write_radiation_line(long timestep);
    void write_radiation_avg_line(long timestep);

    void diag_zond(long timestep);
    //void diag_zond_lineX_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB,long timestep);
    void diag_zond_lineYZ_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, long timestep);
    void write_fields3D(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB,  const long& timestep);
    void write_fields2D(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB,const std::string& axes,  const long& timestep);

};
#endif 	
