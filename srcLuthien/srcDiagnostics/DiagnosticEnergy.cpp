#include "Diagnostic.h"
#include <assert.h>

void DiagData::calc_energy(const Mesh &mesh, const std::vector<ParticlesArray> &species){
  
  for ( auto &sp : species){
    energyParticlesKinetic[sp.name] = sp.get_kinetic_energy();
    energyParticlesInject[sp.name] = sp.get_inject_energy();
  }

  energyFieldE = mesh.get_fieldE_energy();
  energyFieldB = mesh.get_fieldB_energy();

}

void Writer::write_energies(long timestep){
  std::stringstream ss;

  const MPI_Topology &MPIconf = _world.MPIconf;

  if(timestep == 0){
    ss << "Time ";
    for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
      ss << "Area_" << it->first << " ";
    }
    for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
      ss << "Injection_" << it->first << " ";
    }
    ss << "Area_E^2 " << "Area_B^2 " 
       << "powerRadXmin "<< "powerRadXmax "<< "powerRadYmin " << "powerRadYmax "<< "powerRadZmin " << "powerRadZmax " 
       << "powerRadAvgXmin "<< "powerRadAvgXmax "<< "powerRadAvgYmin "<< "powerRadAvgYmax "<< "powerRadAvgZmin "<< "powerRadAvgZmax\n";
  }
  
  ss << timestep*Dt << " ";
    
  for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
    double energyParticles = it->second;
    MPI_Allreduce ( MPI_IN_PLACE, &energyParticles, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
    ss << energyParticles << " ";
  }
  
  for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
    double energyInject = it->second;
    MPI_Allreduce ( MPI_IN_PLACE, &energyInject, 1, MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );
    ss << energyInject << " ";
  }
  std::vector<double> vecEnergy = { diagData.energyFieldE, diagData.energyFieldB,
              diagData.powerRad.x_min, diagData.powerRad.x_max,
              diagData.powerRad.y_min, diagData.powerRad.y_max,
              diagData.powerRad.z_min, diagData.powerRad.z_max,
              diagData.powerRadAvg.x_min, diagData.powerRadAvg.x_max,
              diagData.powerRadAvg.y_min, diagData.powerRadAvg.y_max,
              diagData.powerRadAvg.z_min, diagData.powerRadAvg.z_max };
  
  MPI_Allreduce ( MPI_IN_PLACE, &vecEnergy[0], vecEnergy.size(), MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );

  for(uint i = 0; i< vecEnergy.size(); ++i){
  ss << vecEnergy[i] << " "; 
  }
  ss <<"\n";
  if( MPIconf.is_master() ){
      fprintf(fDiagEnergies, "%s",  ( ss.str() ).c_str() ); 
      std::cout << ss.str();
  }
    if( MPIconf.is_master() && timestep % TimeStepDelayDiag1D == 0){
      fflush(fDiagEnergies);
    }


}

void DiagData::calc_radiation_Pointing(const Mesh &mesh,const Region &region){
  long i,j,k;
  double3 fieldEAvg;
  double3 fieldBAvg;
  
  powerRad.clear();

  assert(region.numCells.z() <= powerRadLine.x_min.size() && "The analysis of the radiation at the boundary X should be along the axis Z. \n Check the size of the array\n");

  if(region.boundType[0].x() == OPEN){
      i = (zondRadX[0] - region.origin) / Dx;
        for( k = 0; k < region.numCells.z(); k++){
          for( j = 0; j < region.numCells.y(); j++){

            fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
            fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
            powerRadLine.x_min(k) += Dt*Dy*Dz*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
            ///powerRad.x_min += Dt*Dy*Dz*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
          }
          powerRad.x_min += powerRadLine.x_min(k);
      }
    }
    if(region.boundType[1].x() == OPEN){
        i = (zondRadX[1] - region.origin) / Dx;
        for( k = 0; k < region.numCells.z(); k++){
          for( j = 0; j < region.numCells.y() ; j++){
            fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
            fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
            powerRadLine.x_max(k) += Dt*Dy*Dz*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
            //powerRad.x_max  += Dt*Dy*Dz*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
          }
          powerRad.x_max += powerRadLine.x_max(k);

        }
    }
    
    j = zondRadY[0] / Dy;
    for (i = 0; i < region.numCells.x() ; i++){
      for (k = 0; k < region.numCells.z() ; k++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
        //powerRadLine.y_min(i) += Dt*Dx * fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
        powerRadLine.y_min(i) += Dt*Dx*Dz* fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
      }
      powerRad.y_min += powerRadLine.y_min(i);
    }
    j = zondRadY[1] / Dy;
    for (i = 0; i < region.numCells.x() ; i++){
      for (k = 0; k < region.numCells.z() ; k++){

        fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
        powerRadLine.y_max(i) += Dt*Dx * Dz*fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
      }
      powerRad.y_max += powerRadLine.y_max(i);
    }

    k = zondRadZ[0] / Dz;
    for (i = 0; i < region.numCells.x() ; i++){
      for (j = 0; j < region.numCells.y() ; j++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
        //powerRadLine.y_min(i) += Dt*Dx * fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
        powerRadLine.z_min(i) += Dt*Dx * Dy * fabs(fieldEAvg.x()*fieldBAvg.y()-fieldEAvg.y()*fieldBAvg.x()) ;
      }
        powerRad.z_min += powerRadLine.z_min(i);
    }
    k = zondRadZ[1] / Dz;
    for (i = 0; i < region.numCells.x() ; i++){
      for (j = 0; j < region.numCells.y() ; j++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j,k);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j,k);
        powerRadLine.z_max(i) += Dt*Dx * Dy* fabs(fieldEAvg.x()*fieldBAvg.y()-fieldEAvg.y()*fieldBAvg.x());
      }
        powerRad.z_max += powerRadLine.z_max(i);
    } 
}


void DiagData::calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep){
  /*long i,j,t;
  long sizeT = long(2 * PI / Dt);
  long t1 = timestep % sizeT;
  double3 fieldE, fieldB;
  double pRad;
  powerRadAvg.clear();

  if(region.boundType_d1[0] == OPEN){
      i = (zondRadX[0] - region.origin) / Dx;
      for( j = 0; j < region.numCells_d2; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLine.x_min.size_d2(); t++){
            fieldE += fieldEAvgLine.x_min(j,t);
            fieldB += fieldBAvgLine.x_min(j,t);
          }

          fieldE = mesh.get_fieldE_in_cell(i,j)
                              - Dt/(2.*PI)*fieldE;
          fieldB = mesh.get_fieldB_in_cell(i,j)
                              - Dt/(2.*PI)*fieldB;
          powerRadAvgLine.x_min(j,t1) =  Dt*Dy*fabs(fieldB.z()*fieldE.y()-fieldE.z()*fieldB.y());
        }
    }
    if(region.boundType_d1[1] == OPEN){
      i = (zondRadX[1]-region.origin) / Dx;
      for( j = 0; j < region.numCells_d2 ; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLine.x_max.size_d2(); t++){
            fieldE += fieldEAvgLine.x_max(j,t);
            fieldB += fieldBAvgLine.x_max(j,t);
          }
          fieldE = mesh.get_fieldE_in_cell(i,j) 
                            - Dt/(2.*PI)*fieldE;
          fieldB = mesh.get_fieldB_in_cell(i,j)
                             - Dt/(2.*PI)*fieldB;
          powerRadAvgLine.x_max(j,t1) = Dt*Dy*fabs(fieldB.z()*fieldE.y()-fieldE.z()*fieldB.y());

        }
    }
    j = zondRadY[0] / Dy;
    for ( i = 0; i < region.numCells_d1; i++){
         fieldE =  fieldB = 0.;
        for( t =  0; t < fieldEAvgLine.y_min.size_d2(); t++){
            fieldE += fieldEAvgLine.y_min(i,t);
            fieldB += fieldBAvgLine.y_min(i,t);
        }
        fieldE = mesh.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldE;
        fieldB = mesh.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldB;
        powerRadAvgLine.y_min(i,t1) =  Dt*Dx * fabs(fieldE.x()*fieldB.z()-fieldE.z()*fieldB.x());
    }
    j = zondRadY[1] / Dy;
    for ( i = 0; i < region.numCells_d1; i++){
         fieldE =  fieldB = 0.;
        for( t =  0; t < fieldEAvgLine.y_max.size_d2(); t++){
            fieldE += fieldEAvgLine.y_max(i,t);
            fieldB += fieldBAvgLine.y_max(i,t);
        }
        fieldE = mesh.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldE;
        fieldB = mesh.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldB;
        powerRadAvgLine.y_max(i,t1) =  Dt*Dx * fabs(fieldE.x()*fieldB.z()-fieldE.z()*fieldB.x());
    }

    for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      pRad = powerRadAvgLine.y_min.sum_d2(i)/(2*PI);
      powerRadAvg.y_min += pRad;
      pRad = powerRadAvgLine.y_max.sum_d2(i)/(2*PI);
      powerRadAvg.y_max += pRad;
    }
    
    for(i = region.dampCells_d2; i < region.numCells_d2 - region.dampCells_d2; i++){
      pRad = powerRadAvgLine.x_min.sum_d2(i)/(2*PI);
      powerRadAvg.x_min += pRad;        
      pRad = powerRadAvgLine.x_max.sum_d2(i)/(2*PI);
      powerRadAvg.x_max += pRad;     
    }
*/
}

void Writer::write_radiation_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max,fRadZ_min,fRadZ_max;
    MPI_Status status;
    char filename[100];
    int sizeDataX = (region.numCells.x() - region.dampCells[0].x() - region.dampCells[1].x());
   // int sizeDataY = (region.numCells.y() - 2*region.dampCells[0].y());
    int sizeDataZ = (region.numCells.z() - 2*region.dampCells[0].z());
    static Array1D<float> floatDataX(sizeDataX);
    //static float* floatDataY = new float[sizeDataY];
    static Array1D<float> floatDataZ(sizeDataZ);
    static long currentStep = 0; 
    int startWrite, sizeWrite;
    float info;
    long i,indx; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadY_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_min);
        info = float(MPIconf.size_line()*sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_min, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadY_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_max);
        info = float(MPIconf.size_line()*sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_max, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadZ_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadZ_min);
        info = float(MPIconf.size_line()*sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadZ_min, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadZ_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadZ_max);
        info = float(MPIconf.size_line()*sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadZ_max, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadX_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_min);
        info = float(sizeDataZ);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_min, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadX_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_max);
        info = float(sizeDataZ);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_max, 0, &info, 1, MPI_FLOAT, &status);
        }
      MPI_Barrier(MPIconf.comm_line());
    }
      indx = region.dampCells[0].x();
    for( i = 0; i < sizeDataX ;i++){
      floatDataX(i) = float(diagData.powerRadLine.y_min(i+indx) );
    }
    startWrite = MPIconf.rank_line()*sizeDataX*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataX*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fRadY_min, startWrite, &floatDataX.data(0), sizeDataX, MPI_FLOAT, &status);
    
    for( i = 0; i < sizeDataX ;i++){

      floatDataX(i) = float(diagData.powerRadLine.y_max(i+indx) );
    }
    MPI_File_write_at(fRadY_max, startWrite, &floatDataX.data(0), sizeDataX, MPI_FLOAT, &status);

    for( i = 0; i < sizeDataX ;i++){
      floatDataX(i) = float(diagData.powerRadLine.z_min(i+indx) );
    }
    MPI_File_write_at(fRadZ_min, startWrite, &floatDataX.data(0), sizeDataX, MPI_FLOAT, &status);

    for( i = 0; i < sizeDataX;i++){
      floatDataX(i) = float(diagData.powerRadLine.z_max(i+indx) );
    }
    MPI_File_write_at(fRadZ_max, startWrite, &floatDataX.data(0), sizeDataX, MPI_FLOAT, &status);



    startWrite = sizeDataZ*sizeof(float)*currentStep;
    indx = region.dampCells[0].z();
    if(region.boundType[0].x()==OPEN){
        for(i = region.dampCells[0].z(); i < sizeDataZ; i++){
            floatDataZ(i) = float(diagData.powerRadLine.x_min(i+indx) );
        }
        MPI_File_write_at(fRadX_min, startWrite + sizeof(float), &floatDataZ.data(0), sizeDataZ, MPI_FLOAT, &status);
    }

    if(region.boundType[1].x()==OPEN){
        for(i = region.dampCells[0].z(); i < sizeDataZ; i++){
            floatDataZ(i) = float(diagData.powerRadLine.x_max(i+indx) );
        }
        MPI_File_write_at(fRadX_max, startWrite + sizeof(float), &floatDataZ.data(0), sizeDataZ, MPI_FLOAT, &status);
    }

    currentStep++;
    
}

void Writer::write_radiation_avg_line(long timestep){
 /*   const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max;
    MPI_Status status;

    char filename[50];
    int sizeDataX = (region.numCells_d1 - region.dampCells_d1[0] - region.dampCells_d1[0]);
    int sizeDataY = (region.numCells_d2 - 2*region.dampCells_d2);
    static float* floatDataX = new float[sizeDataX];
    static float* floatDataY = new float[sizeDataY];
    static long currentStep = 0; 
    int startWrite, sizeWrite;
    float info;
    long i; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadAvgY_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_min);
        info = float(sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_min, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadAvgY_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_max);
        info = float(sizeDataX);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_max, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgX_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_min);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_min, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgX_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_max);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_max, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      floatDataX[i] = float( diagData.powerRadAvgLine.y_min.sum_d2(i)/(2*PI) );
    }
    startWrite = MPIconf.rank_line()*sizeDataX*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataX*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fRadY_min, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);

    for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      floatDataX[i] = float( diagData.powerRadAvgLine.y_min.sum_d2(i)/(2*PI) );
    }
    MPI_File_write_at(fRadY_max, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);


    startWrite = sizeDataY*sizeof(float)*currentStep;
    
    if(region.boundType_d1[0]==OPEN){
        for(i = region.dampCells_d2; i < region.numCells_d2 - region.dampCells_d2; i++){
            floatDataY[i] = float( diagData.powerRadAvgLine.x_min.sum_d2(i)/(2*PI) );
        }
        MPI_File_write_at(fRadX_min, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    if(region.boundType_d1[1]==OPEN){
        for(i = region.dampCells_d2; i < region.numCells_d2 - region.dampCells_d2; i++){
            //pRad = diagData.powerRadAvgLine.x_max.sum_d2(i)/(2*PI);
            floatDataY[i] = float( diagData.powerRadAvgLine.x_max.sum_d2(i)/(2*PI) );
        }
        MPI_File_write_at(fRadX_max, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    currentStep++;
*/
}

/*
void Energies::calc_radiation_Pointing_avg(const World &world, long timestep){
  long i,j,t;
  long sizeT = long(2 * PI / Dt);
  long t1 = timestep % sizeT;
  const Region &region = world.region; 
  double3 fieldEAvgLeft, fieldEAvgRight, fieldEAvgTop, fieldEAvgBottom;
  double3 fieldBAvgLeft, fieldBAvgRight, fieldBAvgTop, fieldBAvgBottom;

  
  if(region.boundType_d1[0] == OPEN){
      i = (fieldsAvgLine_x1 - region.origin) / Dx;
      for( j = 0; j < region.numCells_d2; ++j){
          fieldEAvgLeft = fieldBAvgLeft = 0.;
          for( t =  0; t < world.fieldEAvgLineLeft.size_d2(); t++){
            fieldEAvgLeft += world.fieldEAvgLineLeft(j,t);
            fieldBAvgLeft += world.fieldBAvgLineLeft(j,t);
          }
          fieldEAvgLeft = world.get_fieldE_in_cell(i,j)
                              - Dt/(2.*PI)*fieldEAvgLeft;
          fieldBAvgLeft = world.get_fieldB_in_cell(i,j)
                              - Dt/(2.*PI)*fieldBAvgLeft;

          powerRadLineAvgLeft(j,t1) = Dt*Dy*fabs(fieldBAvgLeft.z()*fieldEAvgLeft.y()-fieldEAvgLeft.z()*fieldBAvgLeft.y());
        }
    }
    if(region.boundType_d1[1] == OPEN){
      i = (fieldsAvgLine_x2 - region.origin) / Dx;
      for( j = 0; j < region.numCells_d2; ++j){
          fieldE = fieldBAvgRight = 0.;
          for( t =  0; t < world.fieldEAvgLineRight.size_d2(); t++){
            fieldEAvgRight += world.fieldEAvgLineLeft(j,t);
            fieldBAvgRight += world.fieldBAvgLineLeft(j,t);
          }

          fieldEAvgRight = world.get_fieldE_in_cell(i,j) 
                            - Dt/(2.*PI)*fieldEAvgRight;

          fieldBAvgRight = world.get_fieldB_in_cell(i,j)
                             - Dt/(2.*PI)*fieldBAvgRight;
          powerRadLineAvgRight(j,t1) = Dt*Dy*fabs(fieldBAvgRight.z()*fieldEAvgRight.y()-fieldEAvgRight.z()*fieldBAvgRight.y());
        }
    }

    j = fieldsAvgLine_y1 / Dy;
    for (i = 0; i < region.numCells_d1;i++){
         fieldEAvgTop =  fieldBAvgTop = 0.;
        for( t =  0; t < world.fieldEAvgLineRight.size_d2(); t++){
            fieldEAvgTop += world.fieldEAvgLineTop(i,t);
            fieldBAvgTop += world.fieldBAvgLineTop(i,t);
        }
        fieldEAvgTop = world.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldEAvgTop;
        fieldBAvgTop = world.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldBAvgTop;
        powerRadLineAvgTop(i,t1) = Dt*Dx * fabs(fieldEAvgTop.x()*fieldBAvgTop.z()-fieldEAvgTop.z()*fieldBAvgTop.x());
    }
    j = fieldsAvgLine_y2 / Dy;
    for (i = 0; i < region.numCells_d1; ++i){
         fieldEAvgBottom =  fieldBAvgBottom = 0.;
        for( t =  0; t < world.fieldEAvgLineRight.size_d2(); t++){
            fieldEAvgBottom += world.fieldEAvgLineTop(i,t);
            fieldBAvgBottom += world.fieldBAvgLineTop(i,t);
        }
        fieldEAvgBottom = world.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldEAvgBottom;
        fieldBAvgBottom = world.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldBAvgBottom;
        powerRadLineAvgBottom(i,t1) = Dt*Dx * fabs(fieldEAvgBottom.x()*fieldBAvgBottom.z()-fieldEAvgBottom.z()*fieldBAvgBottom.x());
    }
    
}

void RWriter::write_radiation1D(Region &region,long timestep){

      if (MPIconf.RankDepth != 0) return;

    FILE* fDamp;
  char filename[100];
  long i;
     
  sprintf(filename, ".//Fields//Diag1D//PRadTop_%03ld_%03d",timestep / TimeDelay1D,MPIconf.RankLine);
  fDamp = fopen(filename, "w");
  fprintf(fDamp, "%s %s %f\n",  "# x_coord", "Pointing_Radiation y = ",fieldsAvgLine_y1); 
  for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1]; ++i){
    fprintf(fDamp, "%g %g\n",  Dx*i + region.origin- Dx*DampCellsX_glob[0], energy.powerRadLineTop(i));
  }
  fclose(fDamp);
  sprintf(filename, ".//Fields//Diag1D//PRadBottom_%03ld_%03d",timestep / TimeDelay1D,MPIconf.RankLine);
  fDamp = fopen(filename, "w");
  fprintf(fDamp, "%s %s %f\n",  "# x_coord", "Pointing_Radiation y = ",fieldsAvgLine_y1); 
  for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1]; ++i){
    fprintf(fDamp, "%g %g\n",  Dx*i + region.origin- Dx*DampCellsX_glob[0], energy.powerRadLineBottom(i));
  }
  fclose(fDamp);
  if(MPIconf.RankLine==0){
      sprintf(filename, ".//Fields//Diag1D//PRadLeft_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 
      fprintf(fDamp, "%s %s %f\n",  "# y_coord", "Pointing_Radiation, x =", fieldsAvgLine_x1 - Dx*DampCellsX_glob[0]); 
      for(i=0;i<region.numCells_d2- region.dampCells_d2;i++){
        fprintf(fDamp, "%g %g\n",  Dy*i, energy.powerRadLineLeft(i));
      }
      fclose(fDamp);
  }
  
  if(MPIconf.RankLine==MPIconf.SizeLine-1){
      sprintf(filename, ".//Fields//Diag1D//PRadRight_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 
      fprintf(fDamp, "%s %s %f\n",  "# y_coord", "Pointing_Radiation, x =", fieldsAvgLine_x2 - Dx*DampCellsX_glob[0]); 
      for(i = 0; i < region.numCells_d2 - region.dampCells_d2; ++i){
        fprintf(fDamp, "%g %g\n",  Dy*i, energy.powerRadLineRight(i));
      }
      fclose(fDamp);    
  }
  

}

void RWriter::write_radiation_avg1D(Region &region,long timestep){

      if (MPIconf.RankDepth != 0) return;

    FILE* fDamp;
  char filename[100];
  long i,t;
  energy.powerRadAvgLeft = energy.powerRadAvgRight = energy.powerRadAvgTop = energy.powerRadAvgBottom = 0.;
  double pRad;
  sprintf(filename, ".//Fields//Diag1D//PRadAvgTop_%03ld_%03d",timestep / TimeDelay1D,MPIconf.RankLine);
  fDamp = fopen(filename, "w");
  fprintf(fDamp, "%s %s %f\n",  "# x_coord", "Pointing_Radiation y = ",fieldsAvgLine_y1); 
  for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1]; ++i){
        pRad = 0;
    for( t =  0; t < energy.powerRadLineAvgTop.size_d2(); t++){
      pRad += energy.powerRadLineAvgTop(i,t)/ (2.*PI);
    }
    fprintf(fDamp, "%g %g\n",  Dx*i + region.origin- Dx*DampCellsX_glob[0], pRad);
      energy.powerRadAvgTop += pRad;

  }
  fclose(fDamp);
    sprintf(filename, ".//Fields//Diag1D//PRadAvgBottom_%03ld_%03d",timestep / TimeDelay1D,MPIconf.RankLine);
  fDamp = fopen(filename, "w");
  fprintf(fDamp, "%s %s %f\n",  "# x_coord", "Pointing_Radiation y = ",fieldsAvgLine_y1); 
  for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1]; ++i){
        pRad = 0;
    for( t =  0; t < energy.powerRadLineAvgBottom.size_d2(); t++){
      pRad += energy.powerRadLineAvgBottom(i,t)/ (2.*PI);
    }
    fprintf(fDamp, "%g %g\n",  Dx*i + region.origin - Dx*DampCellsX_glob[0], pRad);
      energy.powerRadAvgBottom += pRad;

  }
  fclose(fDamp);
  if(MPIconf.RankLine==0){
      sprintf(filename, ".//Fields//Diag1D//PRadAvgLeft_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 

      fprintf(fDamp, "%s %s %f\n",  "# y_coord", "Pointing_Radiation, x =", fieldsAvgLine_x1 - Dx*DampCellsX_glob[0]); 
      for(i = 0 ; i < region.numCells_d2 - region.dampCells_d2; ++i){
         pRad = 0;
        for( t =  0; t < energy.powerRadLineAvgLeft.size_d2(); t++){
          pRad += energy.powerRadLineAvgLeft(i,t)/ (2.*PI);
        }
        fprintf(fDamp, "%g %g\n",  Dy*i, pRad);
                energy.powerRadAvgLeft += pRad;
      }
      fclose(fDamp);
  }
  
  if(MPIconf.RankLine==MPIconf.SizeLine-1){
      sprintf(filename, ".//Fields//Diag1D//PRadAvgRight_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 

      fprintf(fDamp, "%s %s %f\n",  "# y_coord", "Pointing_Radiation, x =", fieldsAvgLine_x2 - Dx*DampCellsX_glob[0]); 
      for(i = 0; i < region.numCells_d2 - region.dampCells_d2; ++i){
        pRad = 0;
        for( t =  0; t < energy.powerRadLineAvgRight.size_d2(); ++t){
            pRad += energy.powerRadLineAvgRight(i,t) / (2.*PI);
        }
        fprintf(fDamp, "%g %g\n",  Dy*i, pRad);
        energy.powerRadAvgRight += pRad;
      }
      fclose(fDamp);    
  }
  

}*/