#include "Diagnostic.h"
#include <vector>

void Writer::diag_zond(long timestep){
  if (!_world.MPIconf.is_master_depth()) return;
  
  char filename[100];
  static FILE *fZond; 
  long n,i,j,k;
  double xx,yy,zz;  
  int numZonds = zondX[0];
  double3 E,B;


  if (timestep == StartTimeStep){
    sprintf(filename, "./Fields/Zond%03d.dat",_world.MPIconf.rank_line() );
    fZond = fopen(filename, "w");
    fprintf(fZond, "%s ", "## timestep ");
    for (n = 1; n <= numZonds; ++n){
      
      if( ! _world.region.in_region(zondX[n]) ) continue;
      xx = zondX[n] - Dx*DampCellsX_glob[0];
      yy = zondY[n];
      zz = zondZ[n];
      fprintf(fZond, "%s%g%s%g%s%g%s %s%g%s%g%s%g%s %s%g%s%g%s%g%s %s%g%s%g%s%g%s %s%g%s%g%s%g%s %s%g%s%g%s%g%s ", 
          "Ex(",xx,",",yy,",",zz,")","Ey(",xx,",",yy,",",zz,")","Ez(",xx,",",yy,",",zz,")",
          "Bx(",xx,",",yy,",",zz,")","By(",xx,",",yy,",",zz,")","Bz(",xx,",",yy,",",zz,")"  );
    }
    fprintf(fZond, "\n");
  }
  
    fprintf(fZond, "%g ",Dt*timestep);
    
    for (n = 1; n <= numZonds; ++n){
      if( ! _world.region.in_region(zondX[n]) ) continue;
      xx = zondX[n] -  _world.region.origin;
      yy = zondY[n];
      zz = zondZ[n];
      i = long(xx / Dx);
      j = long(yy / Dy);
      k = long(zz / Dz);
      E = _mesh.get_fieldE_in_cell(i,j,k);
      B = _mesh.get_fieldB_in_cell(i,j,k);
      fprintf(fZond, "%g %g %g %g %g %g ",  E.x(), E.y(), E.z(), B.x(), B.y(), B.z() );
    }
    
    fprintf(fZond, "\n");
    if(  timestep % TimeStepDelayDiag1D == 0){
      fflush(fZond);
    }

}

static void write_zond_lineYZ_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, long indy, long indz,
      const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep,long timestep){
    
    MPI_Status status;
    char filename[100];
    auto numCells = (domain.numCells.x() - domain.dampCells[0].x() - domain.dampCells[1].x()) ;
    auto sizeData = 6*numCells;
    long indx;
    
    static Array1D<float>floatData(6*sizeData);
   

    if( timestep == StartTimeStep){
        sprintf(filename, "./Fields/ZondLineYZ_nodeY%04d_nodeZ%04d.bin",(int)indy,(int)indz);
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(numCells*MPIconf.size_line());
    
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( auto i = 0; i < numCells; ++i){
        indx = i + domain.dampCells[0].x();
        floatData(6 * i    ) = float(fieldE(indx,indy,indz).x() );
        floatData(6 * i + 1) = float(fieldE(indx,indy,indz).y() );
        floatData(6 * i + 2) = float(fieldE(indx,indy,indz).z() );
        floatData(6 * i + 3) = float(fieldB(indx,indy,indz).x() );
        floatData(6 * i + 4) = float(fieldB(indx,indy,indz).y() );
        floatData(6 * i + 5) = float(fieldB(indx,indy,indz).z() );
    }
  
    int startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    int sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    
    startWrite += sizeWrite*currentStep;
    
    MPI_File_write_at(fZondLine, startWrite, &floatData.data(0), sizeData, MPI_FLOAT, &status);
      
    //delete[] floatData;
}

void Writer::diag_zond_lineYZ_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, long timestep){
  auto numZonds = zondLineY[0];
  if (!_world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  
  long indy,indz;
  static std::vector<MPI_File> fZondLine;
  static std::vector<long> currentStep; 
  if( timestep == StartTimeStep){
    for (auto n = 0; n < numZonds; ++n ){
        MPI_File file;
        fZondLine.push_back(file);
        currentStep.push_back(0);
    }
  }
   
  for (auto n = 0; n < numZonds; ++n ){
    indy = long( zondLineY[n+1] / Dy);
    indz = long( zondLineZ[n+1] / Dz);
    write_zond_lineYZ_bin(_mesh.fieldE, _mesh.fieldB, indy, indz,_world.MPIconf, _world.region, fZondLine[n], currentStep[n],timestep);
    currentStep[n]++;
  }
}
/*
static void write_zond_lineX_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, long indx,
               const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep,long timestep){
    
    MPI_Status status;
    char filename[50];
    int numCells = (domain.numCells_d2 - domain.dampCells_d2) ;
    auto sizeData = 6*numCells;

    std::cout<<sizeData<<"\n";
    static float* floatData = new float[sizeData];

    //static long currentStep = 0; 

    if( timestep == StartTimeStep){
        sprintf(filename, "./Fields/ZondLineX_node%04d.bin",(int)indx + int(domain.origin/Dx) );

        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(numCells);
    
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( auto j = 0; j < numCells; ++j){
        floatData[6 * j    ] = float(fieldE(indx,j).x() );
        floatData[6 * j + 1] = float(fieldE(indx,j).y() );
        floatData[6 * j + 2] = float(fieldE(indx,j).z() );
        floatData[6 * j + 3] = float(fieldB(indx,j).x() );
        floatData[6 * j + 4] = float(fieldB(indx,j).y() );
        floatData[6 * j + 5] = float(fieldB(indx,j).z() );
    }
  
    int startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    int sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fZondLine, startWrite ,floatData, sizeData, MPI_FLOAT, &status);
   // delete[] floatData;

}

void Writer::diag_zond_lineX_bin(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, long timestep){
 auto numZonds = zondLineX[0];
  if (! _world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  long indx;
  static MPI_File fZondLine[5];
  static long currentStep[5] = {0,0,0,0,0}; 

  assert (numZonds < 5 && "Number of files less then number of zonds!\n");
   
  for (auto n = 0; n < numZonds; ++n ){
    if( ! _world.region.in_region(zondLineX[n+1]) ) continue;
    indx = long( (zondLineX[n+1] -  _world.region.origin) / Dx);
    write_zond_lineX_bin(_mesh.fieldE, _mesh.fieldB, indx, _world.MPIconf, _world.region, fZondLine[n], currentStep[n],timestep);
    currentStep[n]++;
  }
  
}*/

void Writer::write_fields2D(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, const std::string& axes, const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    char filename[60];
    float info;    
    MPI_File fField2D;
    MPI_Status status;

    if (axes != "xy" && axes !="xz") return;

    

    long size_x = fieldE.size().x() - ADD_NODES;
    long size_y = fieldE.size().y();
    long size_z = fieldE.size().z();
    long size1 = size_x;
    long size2;
    if (axes == "xy"){
      size2 = size_y;
      sprintf(filename, ".//Fields//Diag2D//Field2Dxy%03ld",timestep / TimeStepDelayDiag2D);
    }
    else if (axes == "xz"){
      sprintf(filename, ".//Fields//Diag2D//Field2Dxz%03ld",timestep / TimeStepDelayDiag2D); 
      size2 = size_z;
    } else return;
    int sizeData = size1 * size2;
    long sizeWriteField;
    long indx;
    float* floatData[6];

    for(auto i = 0; i<6; i++){
        floatData[i] = new float[size1*size2];
    }

    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField2D);
    if (axes == "xy"){
      long k = size_z/2;
      for( auto i = 0; i < size_x; i++ ){
        for( auto j = 0; j < size_y; j++ ){
            indx = i*size_y + j;
              floatData[0][indx] = float(fieldE(i,j,k).x() );
              floatData[1][indx] = float(fieldE(i,j,k).y() );
              floatData[2][indx] = float(fieldE(i,j,k).z() );
              floatData[3][indx] = float(fieldB(i,j,k).x() );
              floatData[4][indx] = float(fieldB(i,j,k).y() );
              floatData[5][indx] = float(fieldB(i,j,k).z() );
        }
      }
    }
    else{
     long j = size_y/2;
      for( auto i = 0; i < size_x; i++ ){
          for( auto k = 0; k < size_z; k++ ){
            indx = i*size_z + k;
              floatData[0][indx] = float(fieldE(i,j,k).x() );
              floatData[1][indx] = float(fieldE(i,j,k).y() );
              floatData[2][indx] = float(fieldE(i,j,k).z() );
              floatData[3][indx] = float(fieldB(i,j,k).x() );
              floatData[4][indx] = float(fieldB(i,j,k).y() );
              floatData[5][indx] = float(fieldB(i,j,k).z() );
          }
      }
    }


    auto sumSize_x = _world.MPIconf.accum_sum_line(size1);
    
    long startWrite = sumSize_x * size2 * sizeof(float) + 2 * sizeof(float);

    if(_world.MPIconf.is_last_line()){
        info = float(sumSize_x + size1);
        MPI_File_write_at(fField2D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size2);
        MPI_File_write_at(fField2D, sizeof(float), &info, 1, MPI_FLOAT, &status);
        sizeWriteField = (sumSize_x + size1) * size2 * sizeof(float);
    }
    

    MPI_Bcast(&sizeWriteField, 1, MPI_LONG, _world.MPIconf.last_line(), _world.MPIconf.comm_line() );

    for(auto i = 0; i<6; ++i){
        MPI_File_write_at(fField2D, startWrite + sizeWriteField*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField2D);
    
    for(auto i = 0; i<6; i++){
        delete[] floatData[i];
    }

}

void Writer::write_fields3D(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    
    MPI_File fField3D;
    MPI_Status status;
    
    char filename[100];
    float info;
    long size_x = fieldE.size().x() - ADD_NODES;
    long size_y = fieldE.size().y();
    long size_z = fieldE.size().z();
    int sizeData = size_x *size_y * size_z;
    long sizeWriteField;
    long indx;
    float* floatData[6];

    for(auto i = 0; i<6; i++){
        floatData[i] = new float[size_x*size_y*size_z];
    }

    sprintf(filename, ".//Fields//Diag3D//Field3D%03ld",timestep / TimeStepDelayDiag2D);

    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField3D);

    for( auto i = 0; i < size_x; i++ ){
      for( auto j = 0; j < size_y; j++ ){
        for( auto k = 0; k < size_z; k++ ){
          indx = i*size_y*size_z + j*size_z + k;
            floatData[0][indx] = float(fieldE(i,j,k).x() );
            floatData[1][indx] = float(fieldE(i,j,k).y() );
            floatData[2][indx] = float(fieldE(i,j,k).z() );
            floatData[3][indx] = float(fieldB(i,j,k).x() );
            floatData[4][indx] = float(fieldB(i,j,k).y() );
            floatData[5][indx] = float(fieldB(i,j,k).z() );
        }
      }
    }
    
    auto sumSize_x = _world.MPIconf.accum_sum_line(size_x);
    
    long startWrite = sumSize_x * size_y * size_z* sizeof(float) + 3 * sizeof(float);

    if(_world.MPIconf.is_last_line()){
        info = float(sumSize_x + size_x);
        MPI_File_write_at(fField3D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size_y);
        MPI_File_write_at(fField3D, sizeof(float), &info, 1, MPI_FLOAT, &status);
        info = float(size_z);
        MPI_File_write_at(fField3D, 2*sizeof(float), &info, 1, MPI_FLOAT, &status);
        sizeWriteField = (sumSize_x + size_x) * size_y * size_z * sizeof(float);
    }
    

    MPI_Bcast(&sizeWriteField, 1, MPI_LONG, _world.MPIconf.last_line(), _world.MPIconf.comm_line() );

    //int startWrite = _world.MPIconf.rank_line()*(PlasmaCellsX_glob/_world.MPIconf.size_line())*size_y*sizeof(float) + 2*sizeof(float);
    
    //if(!_world.MPIconf.is_first_line() ){ 
    //  startWrite += DampCellsX_glob[0] * size_y * sizeof(float);
    //}
//    int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    //int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    
    for(auto i = 0; i<6; ++i){
        MPI_File_write_at(fField3D, startWrite + sizeWriteField*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField3D);
    
    for(auto i = 0; i<6; i++){
        delete[] floatData[i];
    }

}
/*
void Writer::write_fields2D(const Array3D<double3>& fieldE, const Array3D<double3>& fieldB, const std::string& axes ,const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    
    MPI_File fField3D;
    MPI_Status status;
    
    char filename[50];
    float info;

    long size_x = fieldE.size().x() - ADD_NODES;
    long size_y = fieldE.size().y();
    long size_z = fieldE.size().z();
    int sizeData = size_x *size_y * size_z;
    long sizeWriteField;
    long indx;
    float* floatData[6];

    for(auto i = 0; i<6; i++){
        floatData[i] = new float[size_x*size_y*size_z];
    }

    sprintf(filename, ".//Fields//Diag2D//Field2Dxy%03ld",timestep / TimeStepDelayDiag2D);
    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField3D);

    for( auto i = 0; i < size_x; i++ ){
      for( auto j = 0; j < size_y; j++ ){
        for( auto k = 0; k < size_z; k++ ){
          indx = i*size_y*size_z + j*size_z + k;
            floatData[0][indx] = float(fieldE(i,j,k).x() );
            floatData[1][indx] = float(fieldE(i,j,k).y() );
            floatData[2][indx] = float(fieldE(i,j,k).z() );
            floatData[3][indx] = float(fieldB(i,j,k).x() );
            floatData[4][indx] = float(fieldB(i,j,k).y() );
            floatData[5][indx] = float(fieldB(i,j,k).z() );
        }
      }
    }
    
    auto sumSize_x = _world.MPIconf.accum_sum_line(size_x);
    
    long startWrite = sumSize_x * size_y * size_z* sizeof(float) + 3 * sizeof(float);

    if(_world.MPIconf.is_last_line()){
        info = float(sumSize_x + size_x);
        MPI_File_write_at(fField3D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size_y);
        MPI_File_write_at(fField3D, sizeof(float), &info, 1, MPI_FLOAT, &status);
        info = float(size_z);
        MPI_File_write_at(fField3D, 2*sizeof(float), &info, 1, MPI_FLOAT, &status);
        sizeWriteField = (sumSize_x + size_x) * size_y * size_z * sizeof(float);
    }
    

    MPI_Bcast(&sizeWriteField, 1, MPI_LONG, _world.MPIconf.last_line(), _world.MPIconf.comm_line() );

    //int startWrite = _world.MPIconf.rank_line()*(PlasmaCellsX_glob/_world.MPIconf.size_line())*size_y*sizeof(float) + 2*sizeof(float);
    
    //if(!_world.MPIconf.is_first_line() ){ 
    //  startWrite += DampCellsX_glob[0] * size_y * sizeof(float);
    //}
//    int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    //int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    
    for(auto i = 0; i<6; ++i){
        MPI_File_write_at(fField3D, startWrite + sizeWriteField*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField3D);
    
    for(auto i = 0; i<6; i++){
        delete[] floatData[i];
    }

}*/