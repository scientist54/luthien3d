#include "Diagnostic.h"


void Writer::output(long timestep){
    diagData.calc_radiation_Pointing(_mesh,_world.region);
   // diagData.calc_fields_avg_line(_mesh,_world,timestep);
   // diagData.calc_radiation_Pointing_avg(_mesh,_world.region,timestep);

    if (timestep % TimeStepDelayDiag2D == 0){
        write_particles2D(timestep);
        write_fields2D(_mesh.fieldE, _mesh.fieldB,"xy",timestep);
        write_fields2D(_mesh.fieldE, _mesh.fieldB,"xz",timestep);
    }
    if (timestep % TimeStepDelayDiag3D == 0){
        write_particles3D(timestep);
        write_fields3D(_mesh.fieldE, _mesh.fieldB,timestep);
    }
    if (timestep % 2 == 0){

      diag_zond(timestep);
    //  diag_zond_lineX_bin(_mesh.fieldE, _mesh.fieldB, timestep);
      diag_zond_lineYZ_bin(_mesh.fieldE, _mesh.fieldB, timestep);
    }

    if (timestep % TimeStepDelayDiag1D == 0){

        write_radiation_line(timestep);
     //   write_radiation_avg_line(timestep);
        diagData.calc_energy(_mesh,_species);
        write_energies(timestep);        
    }

    if (timestep % RecTimeStep == 0 && timestep != StartTimeStep){
        for (const auto& sp: _species){
            sp.write_recovery(_world.MPIconf);
        }
        _mesh.write_recovery(_world.MPIconf);
    }

}


void make_folders(){
    mkdir(".//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag3D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag2D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag1D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Anime", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Performance", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    std::cout << "Create folders for output...\n";
}

Writer::Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species) : 
    _world(world),_mesh(mesh),_species(species),diagData(world.region) {
  if( !_world.MPIconf.is_master() ) return; 
  
  make_folders(); 
  for( const auto &sp : _species){
    mkdir((".//Particles//" + sp.name).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((".//Particles//" + sp.name+"//Diag1D").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((".//Particles//" + sp.name+"//Diag2D").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((".//Particles//" + sp.name+"//Diag3D").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }

  fDiagEnergies = fopen("Energies.dat", "w");
  
}


void DiagData::calc_fields_avg_line(const Mesh& mesh,const World& world, long timestep){
  /*long i,j,t;
  long sizeT = long(2 * PI / Dt);
  t = timestep % sizeT;
  auto size_x = mesh.fieldE.size_d1();
  auto size_y = mesh.fieldE.size_d2();

    
    j = zondRadY[0] / Dy;  
    for (i = 0; i < size_x - ADD_NODES;++i){
      fieldEAvgLine.y_min(i,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.y_min(i,t) = mesh.get_fieldB_in_cell(i,j);
    }
    j = zondRadY[1] / Dy;  
    for (i = 0; i < size_x - ADD_NODES;++i){
      fieldEAvgLine.y_max(i,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.y_max(i,t) = mesh.get_fieldB_in_cell(i,j);
    } 
    
    i = (zondRadX[0] - world.region.origin) / Dy; 
    if (! world.region.in_region(zondRadX[0])) return;
    for (j = 0; j < size_y - ADD_NODES; ++j){
      fieldEAvgLine.x_min(j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.x_min(j,t) = mesh.get_fieldB_in_cell(i,j);
    }

    
  i = (zondRadX[1] - world.region.origin) / Dx;
  if (!world.region.in_region(zondRadX[1])) return;
    for (j = 0; j < size_y - ADD_NODES; ++j){
      fieldEAvgLine.x_max(j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.x_max(j,t) = mesh.get_fieldB_in_cell(i,j);
    }
*/
}
/*
void RWriter::write_array2D(const Array2D<double>& data, const char* filename){
    MPI_File fData2D;
    MPI_Status status;
    long size1 = data.size_d1() - ADD_NODES;
    long size2 = data.size_d2();
    long sumSize1;
    float info;
    int sizeData = size1*size2;
    
    if ( !MPIconf.IsMasterDepth() ) return;

    float* floatData = new float[size1*size2];

    MPI_File_open(MPIconf.CommLine, filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fData2D);
    
  for( auto i = 0; i < size1; ++i ){
     for( auto j = 0; j < size2; ++j ){
        floatData[i*size2 + j] = float(data(i,j));
    }
  }


    sumSize1 = MPIconf.accum_sum_line(size1);

    if( MPIconf.RankLine == MPIconf.SizeLine -1 ){
          info = float(sumSize1 + size1);
          MPI_File_write_at(fData2D, 0,&info, 1,MPI_FLOAT, &status);
          info = float(size2);
          MPI_File_write_at(fData2D, sizeof(float),&info, 1,MPI_FLOAT, &status);
    }
    
    long startWrite = sumSize1 * size2 * sizeof(float) + 2 * sizeof(float);
        
    MPI_File_write_at(fData2D, startWrite, floatData, sizeData, MPI_FLOAT, &status);
    MPI_File_close(&fData2D);
  
  delete[] floatData;

  
}*/

void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf){
    MPI_File fData2D;
    MPI_Status status;

    long sumSize1;
    float info;
    
    if ( !MPIconf.is_master_depth() ) return;

    Array2D<float> floatData(size1,size2);
    long sizeData = floatData.capacity();

    MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fData2D);
    
    for( auto i = 0; i < size1; ++i ){
       for( auto j = 0; j < size2; ++j ){
          floatData(i,j) = float(data(i,j));
      }
    }


    sumSize1 = MPIconf.accum_sum_line(size1);

    if( MPIconf.is_last_line() ){
          info = float(sumSize1 + size1);
          MPI_File_write_at(fData2D, 0,&info, 1,MPI_FLOAT, &status);
          info = float(size2);
          MPI_File_write_at(fData2D, sizeof(float),&info, 1,MPI_FLOAT, &status);
    }
    long startWrite = sumSize1 * size2 * sizeof(float) + 2 * sizeof(float);
        
    MPI_File_write_at(fData2D, startWrite, &floatData.data(0), sizeData, MPI_FLOAT, &status);
    MPI_File_close(&fData2D);
  
}
void write_array3D(const Array3D<double>& data, long size1, long size2, long size3,const char* filename, const MPI_Topology& MPIconf){
    MPI_File fData3D;
    MPI_Status status;
    long sumSize1,indx;
    float info;
    
    if ( !MPIconf.is_master_depth() ) return;

    Array3D<float> floatData(size1,size2,size3);
    int sizeData = size1*size2*size3;

    MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fData3D);

    for( auto i = 0; i < size1; ++i ){
       for( auto j = 0; j < size2; ++j ){
         for( auto k = 0; k < size3; ++k ){
            floatData(i,j,k) = float(data(i,j,k));
        }
      }
    }


    sumSize1 = MPIconf.accum_sum_line(size1);

    if( MPIconf.is_last_line() ){
          info = float(sumSize1 + size1);
          MPI_File_write_at(fData3D, 0,&info, 1,MPI_FLOAT, &status);
          info = float(size2);
          MPI_File_write_at(fData3D, sizeof(float),&info, 1,MPI_FLOAT, &status);
          info = float(size3);
          MPI_File_write_at(fData3D, 2*sizeof(float),&info, 1,MPI_FLOAT, &status);
    }
    long startWrite = sumSize1 * size2 * size3 * sizeof(float) + 3 * sizeof(float);
        
    MPI_File_write_at(fData3D, startWrite, &floatData.data(0), sizeData, MPI_FLOAT, &status);
    MPI_File_close(&fData3D);

}
