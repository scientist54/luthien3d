# Python script for setting parameters
import pprint
import sys
sys.path.insert(0, "./Scripts")
from setInitParams import *

DirName = "Res_3D_pic_test_es2"


DampType = enum("NONE","DAMP","PML")

DAMP_FIELDS = DampType.DAMP

BoundType = enum("NONE","PERIODIC","OPEN","NEIGHBOUR")

BoundTypeX_glob = [BoundType.OPEN, BoundType.OPEN]
BoundTypeY_glob = [BoundType.OPEN, BoundType.OPEN]
BoundTypeZ_glob = [BoundType.OPEN, BoundType.OPEN]


Queue = "home" # type of queue
Cluster = "nsu" # cluster name (spb, nsu, sscc)

####
RECOVERY = 0

DEBUG = 0

SHAPE = 1  # 1 - PIC, 2 - Parabolic e.t.c

IONIZATION = 0 # Fields ionization

PARTICLE_MASS = False #True ### Particle has individual mass
PARTICLE_MPW = False ### Particle has individual macro particle weight

UPD_PARTICLES = 1

UPD_FIELDS = 1 # Update Fields (for Debug)


#####

NumProcs = 4 # number of processors
NumAreas = 4 # Number of decomposition region


Dx = 0.1 # step on X
Dy = 0.1 # step on Y
Dz = 0.1 # step on Z

PlasmaCellsX_glob = 200 # Number of cells for Plasma on Z

PlasmaCellsY_glob = 60 # Number of cells for Plasma on R 
PlasmaCellsZ_glob = 60 # Number of cells for Plasma on R 

NumCellsY_glob = 200 # NumbeY of all cells in computation domain on R
NumCellsZ_glob = 200 # NumbeY of all cells in computation domain on R

damp = 20
DampCellsX_glob = [damp,damp] # Number of Damping layer cells on Z
DampCellsXP_glob = [10,10] # Number of Damping layer cells on Z near Plasma 
DampCellsY_glob = [damp,damp] # Number of Damping layer cells on Y
DampCellsZ_glob = [damp,damp] # Number of Damping layer cells on Y

NumCellsX_glob = PlasmaCellsX_glob + DampCellsX_glob[0]+DampCellsX_glob[1] # Number of all cells in computation domain on Z


NumPartPerLine = 0 # Number of particles per line segment cell 
NumPartPerCell = 7 #NumPartPerLine**3 # Number of particles per cell

MaxTime = 500 # in 1/w_p
RecTime = 10090 #


DiagDelay2D = 2 # in 1 / w_p
DiagDelay1D = 1 # in 1 / w_p
DiagDelay3D = 2*DiagDelay2D # in 1 / w_p

DiagDelayEnergy1D = 1 

BUniform = [0., 0.0, 0.0] # in w_c / w_p
BCoil = [0, 0.5*NumCellsX_glob*Dx ,100., 20.] 

###### PHYSIC CONST
PI = 3.141592653589793
me = 9.10938356e-28 # electron mass
ee = 4.80320427e-10 # electron charge
n0 = 5.e15 # particles / cm^3
#n0 = 2.5e18 # particles / cm^3
w_p = (4*PI*n0*ee*ee/me)**0.5
cc = 2.99792458e10 # speed on light cm/sec 
MC2 = 511.
########



#######################################


Dt =  0.5*min(Dx,Dy)  # time step

MaxTimeStep = int(round(MaxTime/Dt+1))
RecTimeStep = int(round(RecTime/Dt))
StartTimeStep = int(round(RECOVERY/Dt))
TimeStepDelayDiag3D = int(round(DiagDelay3D/Dt))
TimeStepDelayDiag2D = int(round(DiagDelay2D/Dt))
TimeStepDelayDiag1D = int(round(DiagDelay1D/Dt))


#### ZONDS ##############
numZonds = 15
### First number - number of zonds
zondX = [numZonds] + list(PlasmaCellsX_glob / (numZonds + 1) * Dx * (x+1) + Dx* DampCellsX_glob[0] for x in range(numZonds)  )
zondY = [numZonds] + [ Dy * (NumCellsY_glob - DampCellsY_glob[1] - 30) ] * numZonds
zondZ = [numZonds] + [ Dz * (NumCellsZ_glob - DampCellsZ_glob[1] - 30) ] * numZonds

if not ( len(zondX) == len(zondY) and len(zondY) == len(zondZ) ):
  print("Diff numbers of zonds coord\n")
  exit()

### First number - number of zonds
centerY = 0.5*Dy*NumCellsY_glob
centerZ = 0.5*Dz*NumCellsZ_glob
zondLineY = [1,centerY, centerY+20*Dy, Dy * (NumCellsY_glob - DampCellsY_glob[1] - 30)]
zondLineZ = [zondLineY[0],centerZ, centerZ+20*Dz, Dz * (NumCellsZ_glob - DampCellsZ_glob[1] - 30)]

zondLineX = [0.,Dx * (DampCellsX_glob[0] + 10)]

########################################
#### Coords for radiation diagnostic
zondRadY = [Dy * (NumCellsY_glob - DampCellsY_glob[1] - 30),Dy * ( DampCellsY_glob[1] + 30) ]
zondRadZ = [Dz * (NumCellsZ_glob - DampCellsZ_glob[1] - 30),Dz * ( DampCellsZ_glob[1] + 30) ]
zondRadX = [Dx * (DampCellsX_glob[0] + 10), Dx * (NumCellsX_glob - DampCellsX_glob[1] - 10)]



PxMax = 200 // NumAreas
PpMax = 800

MaxSizeOfParts=3*NumPartPerCell*PlasmaCellsX_glob*PlasmaCellsY_glob*PlasmaCellsZ_glob/NumProcs+1


NumOfPartSpecies = 0

PartParams = {} # 
 # 
isVelDiag = 0

Vb = 0.3#79 #0.995

PName="Neutrals"

Exist = False
PartDict = {}
PartDict["Charge"] = 0.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
Pot_I = 0.02459 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 1.
PartDict["Temperature"] = 0;# (0.014/512.)**0.5
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["WidthY"] = PlasmaCellsY_glob*Dy
PartDict["WidthZ"] = PlasmaCellsZ_glob*Dz
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 1
InitDist = "StrictUniformCircle"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]



if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Electrons"

Exist = True
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 1.0 #0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1.0
PartDict["Temperature"] = (0.014/512.)**0.5
PartDict["Px_max"] = 1.e-1 # 
PartDict["Px_min"] = -1.e-1 #
PartDict["WidthY"] = PlasmaCellsY_glob*Dy
PartDict["WidthZ"] = PlasmaCellsZ_glob*Dz
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["BoundResumption"] = 1
InitDist = "None"
InitDist = "UniformCircle"
#InitDist = "UniformCosX_dn_k"
dn0 = 0.1
k0 = 1./Vb
PartDict["DistParams"] = [str(InitDist), dn0, k0]


if Exist:
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="Ions"

Exist = False#True
PartDict = {}
PartDict["Charge"] = 1.0
PartDict["Density"] = 1.0
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1836.0 * 4
PartDict["Temperature"] = 0.0
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["WidthY"] = PlasmaCellsY_glob*Dy
PartDict["WidthZ"] = PlasmaCellsZ_glob*Dz
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0
PartDict["SmoothMassMax"] = 30
PartDict["SmoothMassSize"] = 15
PartDict["BoundResumption"] = 1

InitDist = "StrictUniformCircle"
#InitDist = "None"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]
Pot_I = 0.05442 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 2.

#SmoothMassMax = 800.
#SmoothMassSize = 0.15*PlasmaCellsZ_glob * Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Ions2"

Exist = False
PartDict = {}
PartDict["Charge"] = 2.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
PartDict["Temperature"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["WidthY"] = PlasmaCellsY_glob*Dy
PartDict["WidthZ"] = PlasmaCellsZ_glob*Dz
PartDict["Shift"] = 0.0
PartDict["BoundResumption"] = 1

InitDist = "None" #"StrictUniformCircle"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]

SmoothMassMax = 40.
SmoothMassSize = 10. #0.15*PlasmaCellsZ_glob*Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)



###### GLOBAL BEAMS PARAMETERS #####################
InjType = 0 # 0 - random inject, 1 - periodic strit inject
InjectSmoothTime = 0.001


SourceType = enum("NONE","FOCUSED_GAUSS","UNIFORM")
#### FOCUSED BEAMS 

Lmax = 10. / (cc / w_p) # cm / c/w_p
Larea = 0.5*PlasmaCellsX_glob*Dx
Rmax = 0.1* 2.5 / (cc / w_p)  # cm / c/w_p
Rfocus = 0.05  / (cc / w_p) # cm / c/w_p
Xfocus = 0.5 * Dx * PlasmaCellsX_glob



PName="BeamLeft"

Exist = False
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.01
PartDict["Velocity"] = 0.8#Vb
PartDict["Mass"] = 1.0
PartDict["Temperature"] = 0#(5.014/512.)**0.5
PartDict["Px_max"] = 10.0
PartDict["Px_min"] = 0.0
PartDict["WidthY"] = PlasmaCellsY_glob*Dy#Rfocus * 0.5
PartDict["WidthZ"] = PlasmaCellsZ_glob*Dz#Rfocus * 0.5
PartDict["Focus"] = Dx * PlasmaCellsX_glob
PartDict["SourceType"] = SourceType.UNIFORM #FOCUSED_GAUSS
PartDict["SourceAngle"] = 0#PI/10;
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]


if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="BeamRight"

Exist = False
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.01
PartDict["Velocity"] = -Vb
PartDict["Mass"] = 1.0
PartDict["Temperature"] = 0.0
PartDict["Px_max"] = 0.0
PartDict["Px_min"] = -10.0
PartDict["WidthY"] = Rfocus
PartDict["WidthZ"] = Rfocus
PartDict["Focus"] = 0.5 * Dx * PlasmaCellsX_glob
PartDict["SourceType"] = SourceType.FOCUSED_GAUSS
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)
    

### GLOBAL LASERS PARAMETERS ##############################
Las_tau = 3.48
Las_w0 = 25.44
Las_vg = (1. - 1. / (Las_w0**2) )**0.5

LasParams = {} # 

DelayLeft =0.
DelayRight = 0.
LasName="LaserLeft"
Exist = False
LasDict = {} #
LasDict["delay"] = 0#DelayLeft*Las_tau
LasDict["tau"] = Las_tau
LasDict["type"] = "Ez"
LasDict["vg"] = Las_vg
LasDict["a0"] = 0.67
LasDict["sigma0"] = 1.86
LasDict["w0"] = Las_w0
LasDict["focus_x"] =  0.5 * Dx * NumCellsX_glob
LasDict["y0"] =  0.5 * Dy * NumCellsY_glob
LasDict["start_x"] = Dx*DampCellsX_glob[0]

if Exist:
    setParams(LasParams, LasName, LasDict)

LasName="LaserRight"
Exist = False
LasDict = {} #
LasDict["delay"] = 0#DelayRight*Las_tau
LasDict["tau"] = Las_tau
LasDict["vg"] = -Las_vg
LasDict["a0"] = 0.8
LasDict["sigma0"] =  5.28
LasDict["type"] = "Ey"
LasDict["w0"] = Las_w0
LasDict["y0"] =  0.5 * Dy * NumCellsY_glob
LasDict["focus_x"] =  -0.5 * Dx * NumCellsX_glob
LasDict["start_x"] = Dx*PlasmaCellsX_glob + Dx*DampCellsX_glob[0]-Dx

if Exist:
    setParams(LasParams, LasName, LasDict)



#####//////////////////////////////

WorkDir = DirName+"_nx_"+str(PlasmaCellsX_glob)+"_np_"+str(NumPartPerCell )+"_Dx_"+str(Dx)+"_DelayLeft_"+str(DelayLeft)+"_DelayRight_"+str(DelayRight)


if SHAPE < 3:
    SHAPE_SIZE = 2
    CELLS_SHIFT = 1
else:
    SHAPE_SIZE = 3
    CELLS_SHIFT = 2    

ADD_NODES = 2 * CELLS_SHIFT + 1


if PlasmaCellsX_glob % NumAreas != 0:
	print("***********************************************")
	print("WARNING!!! Domain decomposition is not correct!!")
	print("***********************************************")

###////////////////////////////////

DefineParams = []
SysParams = []

setConst(SysParams,'const long','NumProcs',[NumProcs],None)
setConst(SysParams,'const long','NumAreas',[NumAreas],None)

setConst(SysParams,'const double','Dx',[Dx],None)
setConst(SysParams,'const double','Dy',[Dy],None)
setConst(SysParams,'const double','Dz',[Dz],None)
setConst(SysParams,'const double','Dt',[Dt],None)

setConst(SysParams,'const long','NumCellsX_glob',[NumCellsX_glob],None)
setConst(SysParams,'const long','NumCellsY_glob',[NumCellsY_glob],None)
setConst(SysParams,'const long','NumCellsZ_glob',[NumCellsZ_glob],None)
setConst(SysParams,'const long','NumCellsXmax_glob',[NumCellsX_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','NumCellsYmax_glob',[NumCellsY_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','DampCellsX_glob',DampCellsX_glob,None)
setConst(SysParams,'const long','DampCellsXP_glob',DampCellsXP_glob,None)
setConst(SysParams,'const long','DampCellsY_glob',DampCellsY_glob,None)
setConst(SysParams,'const long','DampCellsZ_glob',DampCellsZ_glob,None)
setConst(SysParams,'const long','PlasmaCellsY_glob',[PlasmaCellsY_glob],None)
setConst(SysParams,'const long','PlasmaCellsX_glob',[PlasmaCellsX_glob],None)
setConst(SysParams,'const long','PlasmaCellsZ_glob',[PlasmaCellsZ_glob],None)

setConst(SysParams,'const long','NumOfPartSpecies',[NumOfPartSpecies],None)
setConst(SysParams,'const long','NumPartPerLine ',[NumPartPerLine ],None)
setConst(SysParams,'const long','NumPartPerCell',[NumPartPerCell],None)

setConst(SysParams,'const long','MaxTimeStep',[MaxTimeStep],None)
setConst(SysParams,'const long','RecTimeStep',[RecTimeStep],None)
setConst(SysParams,'const long','StartTimeStep',[StartTimeStep],None)
setConst(SysParams,'const long','TimeStepDelayDiag1D',[TimeStepDelayDiag1D],None)
setConst(SysParams,'const long','TimeStepDelayDiag2D',[TimeStepDelayDiag2D],None)
setConst(SysParams,'const long','TimeStepDelayDiag3D',[TimeStepDelayDiag3D],None)
#setConst(SysParams,'const long','EnergyOutputDelay1D',[EnergyOutputDelay1D],None)

setConst(SysParams,'const double','InjectSmoothTime',[InjectSmoothTime],None)

setConst(SysParams,'const double','BUniform',BUniform,None)
setConst(SysParams,'const double','BCoil',BCoil,None)
setConst(SysParams,'const long','BoundTypeY_glob',BoundTypeY_glob,None)
setConst(SysParams,'const long','BoundTypeZ_glob',BoundTypeZ_glob,None)
setConst(SysParams,'const long','BoundTypeX_glob',BoundTypeX_glob,None)

setConst(SysParams,'const double','MC2',[MC2],None)
setConst(SysParams,'const double','n0',[n0],None)
setConst(SysParams,'const double','zondZ',zondZ,None)
setConst(SysParams,'const double','zondY',zondY,None)
setConst(SysParams,'const double','zondX',zondX,None)
setConst(SysParams,'const double','zondLineX',zondLineX,None)
setConst(SysParams,'const double','zondLineY',zondLineY,None)
setConst(SysParams,'const double','zondLineZ',zondLineZ,None)

setConst(SysParams,'const double','zondRadY',zondRadY,None)
setConst(SysParams,'const double','zondRadZ',zondRadZ,None)
setConst(SysParams,'const double','zondRadX',zondRadX,None)

setConst(SysParams,'const int','isVelDiag',[isVelDiag],None)
setConst(SysParams,'const long','PxMax',[PxMax],None)
setConst(SysParams,'const long','PpMax',[PpMax],None)

setConst(SysParams,'const long','MaxSizeOfParts',[MaxSizeOfParts],None)

setConst(SysParams,'const double','PI',[PI],None)

setConst(SysParams,'const double','Rmax',[Rmax],None)
setConst(SysParams,'const double','Rfocus',[Rfocus],None)
setConst(SysParams,'const double','Lmax',[Lmax],None)
setConst(SysParams,'const double','Larea',[Larea],None)
setConst(SysParams,'const double','SmoothMassMax',[SmoothMassMax],None)
setConst(SysParams,'const double','SmoothMassSize',[SmoothMassSize],None)

if DEBUG == 0:
    setConst(DefineParams,'#define','NDEBUG',[' '],None)
else:
    setConst(DefineParams,'#define','DEBUG',[DEBUG],None)
if PARTICLE_MASS:
    setConst(DefineParams,'#define','PARTICLE_MASS',[1],None)
if PARTICLE_MPW:
    setConst(DefineParams,'#define','PARTICLE_MPW',[1],None)
setConst(DefineParams,'#define','RECOVERY',[RECOVERY],None)
setConst(DefineParams,'#define','SHAPE',[SHAPE],None)
setConst(DefineParams,'#define','SHAPE_SIZE',[SHAPE_SIZE],None)
setConst(DefineParams,'#define','CELLS_SHIFT',[CELLS_SHIFT],None)
setConst(DefineParams,'#define','ADD_NODES',[ADD_NODES],None)
setConst(DefineParams,'#define','IONIZATION',[IONIZATION],None)

setConst(DefineParams,'#define','DEBUG',[DEBUG],None)
setConst(DefineParams,'#define','UPD_FIELDS',[UPD_FIELDS],None)
setConst(DefineParams,'#define','UPD_PARTICLES',[UPD_PARTICLES],None)
setConst(DefineParams,'#define','DAMP_FIELDS',[DAMP_FIELDS],None)

setConst(DefineParams,'#define','PML',[DampType.PML],None)
setConst(DefineParams,'#define','DAMP',[DampType.DAMP],None)
setConst(DefineParams,'#define','PERIODIC',[BoundType.PERIODIC],None)
setConst(DefineParams,'#define','OPEN',[BoundType.OPEN],None)
setConst(DefineParams,'#define','NEIGHBOUR',[BoundType.NEIGHBOUR],None)
setConst(DefineParams,'#define','SOURCE_FOCUSED_GAUSS',[SourceType.FOCUSED_GAUSS],None)
setConst(DefineParams,'#define','SOURCE_UNIFORM',[SourceType.UNIFORM],None)
setConst(DefineParams,'#define','SOURCE_NONE',[SourceType.NONE],None)



writeParams("Particles","PartParams.cfg",PartParams)
writeParams("Lasers","LasParams.cfg",LasParams)

writeConst('./srcLuthien/const.h', 'SysParams.cfg', SysParams)
writeDefine('./srcLuthien/defines.h',DefineParams)



f = open('phys.par', 'w')
f.write("w_p = " + str(w_p))
f.write("1/w_p = " + str(1./w_p))
f.close()

f = open('workdir.tmp', 'w')
f.write(WorkDir)
f.close()
f = open('queue.tmp', 'w')
f.write(Queue)
f.close()
f = open('cluster.tmp', 'w')
f.write(Cluster)
f.close()
f = open('proc.tmp', 'w')
f.write(str(NumProcs))
f.close()
